package br.com.ia.oct.mcd.mgmt.register;


public interface MgmtRegister extends OCTRegister {
	public void registerJob (Class job);
	public void registerComponent(Class component);
	public void truncateComponents();
}
