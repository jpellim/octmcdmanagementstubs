package br.com.ia.oct.mcd.loader;

public interface OCTLoaderAdapter {
	public void start() throws Exception;
	public void destroy() throws Exception;
	public void registerAdapter() throws Exception;
}
