package br.com.ia.oct.mcd.scheduler;

import java.util.HashMap;

import org.quartz.Scheduler;

import br.com.ia.oct.mcd.common.entity.Job;

public interface SchedulerManager {
	public boolean startup ();
	public boolean shutdown ();
	public boolean reload ();
	public boolean execJobNow(String uid);
	public boolean scheduleJob (Long id, String jndi, String grupo, String nome, String descricao, String cronTrigger, HashMap<String, Object> params);
	public boolean scheduleJob (Long id, String jndi, String grupo, String nome, String descricao, String cronTrigger);
	public boolean scheduleJob (Job job);
	public Scheduler getScheduler();
	public String shcedulerManagerStatus();

}
