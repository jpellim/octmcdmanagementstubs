package br.com.ia.oct.mcd.scheduler;

import java.io.Serializable;
import java.util.HashMap;

public interface OCTJob extends Serializable {
	public boolean execute (HashMap<String, Object> params) throws Exception;
}
