package br.com.ia.oct.mcd.dataservice;

import java.io.Serializable;
import java.util.List;

import javax.persistence.EntityManager;

import org.hibernate.Session;
import org.hibernate.criterion.Junction;
import org.hibernate.criterion.Order;

public interface DataService<T> {
	public T getByID(Serializable id, Class type);
	public List<T> getList (Class type);
	public List<T> getList (Class type, int maxResult);
	public List<T> getList (Class type, int maxResult, Order order);
	public List<T> getList (Class type, Junction junction);
	public List<T> getList (Class type, Order order, Junction junction);
	public List<T> getList (Class type, int maxResult, Order order, Junction junction);
	public List executeQuery (String nativeQuery);
	public Integer executeUpdateQuery (String nativeQuery);
	public void save (T obj, Class type);
	public T saveObj (T obj, Class type);
	public void saveOrUpdate (T obj, Class type);
	public Session getSession ();
	public EntityManager getEntityManager();
	public void evict (T obj);
}
