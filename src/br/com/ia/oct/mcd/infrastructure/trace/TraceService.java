package br.com.ia.oct.mcd.infrastructure.trace;

import java.util.Calendar;

import br.com.ia.oct.mcd.common.entity.Trace;

/**
 * Componente utilizado para centralizar os eventos de neg�cio do sistema em um reposit�rio �nico
 * @author fernandosapata
 * @ID: 4.3.1
 */
public interface TraceService {
	public void trace (Trace trace);
	public void trace (Long seqUid, String componentID, String message, String details, Calendar date);
	
}
